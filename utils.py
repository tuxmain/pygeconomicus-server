#!/usr/bin/env python3

"""
	This file is part of Pygeconomicus-server.

	Pygeconomicus-server is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Pygeconomicus-server is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pygeconomicus-server.  If not, see <https://www.gnu.org/licenses/>.
"""

import configparser, socket, json, os, time, random, sys

ERROR_OK = 0
ERROR_COM_HEADER = 16 # Communication error: decoding header
ERROR_COM_PROTOCOL = 17 # Communication error: unsupported protocol version
ERROR_COM_LANGUAGE = 18 # Communication error: unknown language
ERROR_COM_JSON = 19 # Communication error: decoding JSON
ERROR_GAME_USEDGAMENAME = 48 # Game error: that game name is already used
ERROR_GAME_MISSNEWGAMENAME = 49 # Game error: miss new game name
ERROR_GAME_MISSGAMENAME = 50 # Game error: miss game name
ERROR_GAME_GAMENOEXIST = 51 # Game error: there is no game with this name
ERROR_GAME_MISSADMINPSW = 52 # Game error: miss admin password
ERROR_GAME_WRONGADMINPSW = 53 # Game error: wrong admin password
ERROR_GAME_PLAYERNOEXIST = 54 # Game error: that player does not exist
ERROR_GAME_WRONGPLAYERPSW = 55 # Game error: wrong player password
ERROR_GAME_MISSPLAYERPSW = 56 # Game error: miss player password
ERROR_GAME_MISSEXCHDATA = 57 # Game error: miss exchanges data
ERROR_GAME_INVEXCHDATA = 58 # Game error: invalid exchange data
ERROR_SRV_NOSOURCE = 128 # Server error: source not available

LOG_INFO = 1
LOG_TRACE = 2
LOG_WARN = 3
LOG_ERROR = 4
LOGMSG_TYPES = {LOG_INFO:"\033[96minfo\033[0m", LOG_TRACE:"\033[39mtrace\033[0m", LOG_WARN:"\033[93mwarn\033[0m", LOG_ERROR:"\033[91merror\033[0m"}

# Default game constants
CONSTS = {
	"N_VALUES": 4, # number of values types
	"N_WVALUES": 1, # number of waiting values types
	"TURN_DURATION": 300, # in seconds
	"N_TURNS": 10, # number of turns in a game
	"TURN_YEARS": 8, # number of years by turn
	"LIFESPAN": 10, # max age
	"N_DIGITS": 0, # number of decimals
	"START_VALUES": 4 # number of random values for beginning
}

# Default server constants
PATH_CONF = "server.ini" # config file relative path
RECBUF = 1024 # reception buffer length
PROTOCOL_VERSIONS = ["geco0"]# supported protocol versions
DEFAULT_PROTOCOL = "geco0"
VERSION = "0.0.0"
HOST = socket.gethostname() # server host name
PORT = 8651 # server port
SOURCE_FILES = ["server.py", "money_free.py", "money_debt.py", "utils.py", "README.md", "LICENSE"]
SOURCE_ARCHIVE = "sourcecode.tar.gz"

games = []
games_index = {}

class ContinueError(Exception):
	def __init__(self):
		pass

# Server functions

def getargv(argv, arg, default=""):
	if arg in argv and len(argv) > argv.index(arg)+1:
		return argv[argv.index(arg)+1]
	else:
		return default

def readConfig():
	conf = configparser.ConfigParser()
	conf.read(PATH_CONF)
	if "Server" in conf:
		if "Host" in conf["Server"]:
			host = conf["Server"]["Host"]
		if "Port" in conf["Server"]:
			port = int(conf["Server"]["Port"])
	if "Game" in conf:
		for key, value in conf["Game"].items():
			CONSTS[key] = value

def encodeData(raw, lng="json", protocol=DEFAULT_PROTOCOL):
	return protocol.encode()+b"/"+lng.encode()+b"/"+hex(len(raw))[2:].encode()+b"\n"+raw

def sendErrorToClient(client, errcode, resp_lng="json"):
	resp_raw = b""
	if resp_lng == "json":
		resp_raw = json.dumps({"error":errcode}).encode()
	client.sendall(encodeData(resp_raw))
	client.close()
	logPrint("Sent error "+str(errcode)+" to client", LOG_TRACE)

def tarSource():
	import tarfile
	def tarReset(tarinfo):
		tarinfo.uid = tarinfo.gid = 0
		tarinfo.uname = tarinfo.gname = "root"
		return tarinfo
	tar = tarfile.open(SOURCE_ARCHIVE, "w:gz")
	for source_file in SOURCE_FILES:
		tar.add(source_file, filter=tarReset)
	tar.close()
	logPrint("Source archive created at '"+SOURCE_ARCHIVE+"'", LOG_INFO)

def getGameName(p_data, client, resp_lng):
	if not "game_name" in p_data:
		sendErrorToClient(client, ERROR_GAME_MISSGAMENAME, resp_lng)# Game error: miss game name
		raise ContinueError
	return p_data["game_name"]

def getGame(g_name, client, resp_lng):
	if not g_name in games_index:
		sendErrorToClient(client, ERROR_GAME_NONAMED, resp_lng)# Game error: there is no game with this name
		raise ContinueError
	return games_index[g_name]

# Money functions

def moneySupply(players):
	r = 0
	for player in players:
		r += player.wallet
	return r

def relative(UD, amount):
	return amount/UD

def logPrint(msg, msgtype):
	print(time.strftime("%Y-%m-%d %H:%M:%S")+" ["+LOGMSG_TYPES[msgtype]+"] "+msg)
