#!/usr/bin/env python3

"""
	This file is part of Pygeconomicus-server.

	Pygeconomicus-server is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Pygeconomicus-server is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pygeconomicus-server.  If not, see <https://www.gnu.org/licenses/>.
"""

import money_free, money_debt
from utils import *

class Player():
	def __init__(self, name, address, psw, uid):
		# Consts
		self.name = name
		self.address = address
		self.psw = psw
		self.uid = uid
		
		# Vars
		self.age = None
		self.values = None
		self.wallet = None
		self.t_values = None
		self.t_wallet = None

class Game():
	def __init__(self, name, public, admin_name, admin_address, admin_psw, consts):
		# Consts
		self.name = name
		self.creation_time = int(time.time())
		self.public = public
		self.admin_name = admin_name
		self.admin_address = admin_address
		self.admin_psw = admin_psw
		self.C = consts
		
		# Vars
		self.players = [] # players list
		self.players_index = {} # players associative index
		self.next_uid = 1
		self.money_system = None # the actual MoneySystem object
		self.turn = None # current turn number
		self.endturn = None # end turn timestamp
		self.values = [None] * self.C["N_VALUES"] # values order (the last is the waiting value)
		self.exchanges = [] # pending exchanges
	
	# Add one player
	# address is ("address", port)
	def addPlayer(self, name, address, psw):
		player = Player(name, address, psw, self.next_uid)
		self.players.append(player)
		self.players_index[self.next_uid] = player
		self.next_uid += 1
		return player
	
	# Remove one player
	def removePlayer(self, uid):
		if uid in self.players_index:
			player = self.players_index.pop(uid)
			self.players.remove(player)
			return player
		return False
	
	# Init one player
	# pass reborn=False at beginning
	# pass reborn=True when the player dies and reborns
	def initPlayer(self, player, reborn):
		if not reborn:
			player.t_values = 0
			player.t_wallet = 0
		
		# Start with random values
		player.values = [0] * self.C["N_VALUES"]
		for i in range(self.C["START_VALUES"]):
			player.values[random.randint(0, self.C["N_VALUES"]-self.C["N_WVALUES"])-1] += 1
		
		# Init in MoneySystem
		player.wallet = 0
		self.money_system.initPlayer(player, reborn)
	
	def feedPlayerReport(self, player):
		player.t_values = player.values
		player.t_wallet = player.wallet
	
	# Init game part
	def startGame(self, money_system):
		self.money_system = money_system.MoneySystem(self.players, self.C)
		self.turn = 0
		self.values = list(range(self.C["N_VALUES"]))
		
		# Init players
		ages = []
		age = 0
		for i in range(len(self.players)):
			ages.append(age)
			age = (age+1) % self.C["LIFESPAN"]
		
		for player in self.players:
			age_p = random.choice(ages)
			player.age = age_p
			ages.remove(age_p)
			self.initPlayer(player, False)
	
	# New turn
	def newTurn(self):
		self.turn += 1
		
		self.money_system.newTurn(self.turn)
	
	# End turn
	def endTurn(self):
		self.money_system.endTurn()
		
		# Kill some players
		for player in self.players:
			player.age += 1
			if player.age >= self.C["LIFESPAN"]:
				self.feedPlayerReport(player)
				player.age = 0
				self.initPlayer(player, True)

if __name__ == "__main__":
	# Config
	
	readConfig()
	if "--help" in sys.argv:
		print("pygeconomicus-server "+VERSION+"\n\
Copyright 2018 Pascal Engélibert (GNU AGPL v3+)\n\
\n\
Options:\n\
  -h <hostname>  Server hostname (default: '"+HOST+"')\n\
  -p <port>      Server port (default: "+str(PORT)+")\n\
  --help         Show this message\n\
  --source       Create source archive (publish modified source code to comply with GNU AGPL)\n\
")
		exit()
	
	if "--source" in sys.argv:
		tarSource()
		exit()
	
	HOST = getargv(sys.argv, "-h", HOST)
	
	try:
		PORT = int(getargv(sys.argv, "-p", PORT))
	except ValueError:
		print("Error: port must be an integer")
		exit(1)

	# Start server

	loop = True
	server_address = (HOST, PORT)
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	sock.bind(server_address)
	sock.listen(1)
	logPrint("Server started at "+str(server_address), LOG_INFO)
	while loop:
		client, addr = sock.accept()
		client.settimeout(5)
		paquet = b""
		p_len_tmp = ""
		p_len = None
		p_data_index = None
		i = 0
		while True:
			try:
				raw = client.recv(RECBUF)
			except socket.timeout:
				break
			if raw:
				for c in raw:
					if p_len == None:
						if c == 10:# LF
							p_len = int(p_len_tmp, 16)
							p_data_index = len(paquet)+1
						elif (c >= 48 and c <= 57) or (c >= 97 and c <= 102) or (c >= 65 and c <= 70):# [0-9a-fA-F]
							p_len_tmp += chr(c)
						else:
							p_len_tmp = ""
					else:
						i += 1
					paquet += bytes([c])
				if p_len != None and i >= p_len:
					break
			else:
				break
		
		p_resp = False
		resp_raw = b""# raw response
		resp = {"error":ERROR_OK}# response
		resp_lng = "json"# response language
		
		# Parse paquet
		try:
			seps = [paquet.index(b"/")]
			p_ver = paquet[:seps[0]]# protocol version
			if not p_ver.decode() in PROTOCOL_VERSIONS:
				sendErrorToClient(client, ERROR_COM_PROTOCOL, resp_lng)# Communication error: unsupported protocol version
				continue
			seps.append(paquet.index(b"/", seps[0]+1))
			seps.append(paquet.index(b"\n", seps[1]+1))
		except ValueError:
			sendErrorToClient(client, ERROR_COM_HEADER, resp_lng)# Communication error: decoding header
			continue
		
		p_lng = paquet[seps[0]+1:seps[1]]# data language
		
		if p_len > 0:
			p_raw = paquet[p_data_index:]
			
			if p_lng == b"json":
				try:
					p_data = json.loads(p_raw)
				except json.JSONDecodeError:
					sendErrorToClient(client, ERROR_COM_JSON, resp_lng)# Communication error: decoding JSON
					continue
				
				if "source" in p_data:
					logPrint("Received source code request", LOG_TRACE)
					p_resp = True
					source_time = os.path.getmtime(SOURCE_ARCHIVE)
					for source_file in SOURCE_FILES:
						if os.path.getmtime(source_file) > source_time:
							tarSource()
							break
					if os.path.isfile(SOURCE_ARCHIVE):
						resp_lng = "raw"
						f = open(SOURCE_ARCHIVE, "rb")
						resp_raw = f.read()
						f.close()
					else:
						logPrint("Source archived but file does not exist: '"+SOURCE_ARCHIVE+"'", LOG_ERROR)
						sendErrorToClient(client, ERROR_SRV_NOSOURCE, resp_lng)# Server error: source not available
						continue
				
				elif "query" in p_data:
					p_resp = True
					
					if "games" in p_data["query"]:
						logPrint("Received games list query", LOG_TRACE)
						resp["games"] = []
						for game in games:
							if game.public:
								gp = {"name":game.name, "creation_time":game.creation_time, "admin_name":game.admin_name, "consts":game.C, "n_players":len(game.players), "money_system":None}
								if game.money_system != None:
									gp["money_system"] = game.money_system.name
								resp["games"].append(gp)
					
					if "game_info" in p_data["query"]:
						logPrint("Received game list query", LOG_TRACE)
						if not "game_name" in p_data:
							sendErrorToClient(client, ERROR_GAME_MISSGAMENAME, resp_lng)# Game error: miss game name
							continue
						g_name = p_data["game_name"]
					
						if not g_name in games_index:
							sendErrorToClient(client, ERROR_GAME_GAMENOEXIST, resp_lng)# Game error: there is no game with this name
							continue
						game = games_index[g_name]
						
						resp["game_info"] = {}
						
						resp["game_info"]["players"] = []
						for player in game.players:
							resp["game_info"]["players"].append({"player_name":player.name, "player_uid":player.uid, "player_address":player.address[0], "player_port":player.address[1]})
					
					if "server_info" in p_data["query"]:
						logPrint("Received server info query", LOG_TRACE)
						resp["server_info"] = {"version":VERSION, "protocol_versions":PROTOCOL_VERSIONS}
				
				if "new_game" in p_data:
					logPrint("Received new game command", LOG_TRACE)
					p_resp = True
					g = p_data["new_game"]
					
					g_admin_address = (addr[0], 8655)
					if "admin_address" in g:
						g_admin_address = [g["admin_address"], g_admin_address[1]]
					if "admin_port" in g:
						g_admin_address = [g_admin_address[0], g["admin_port"]]
					
					g_admin_name = "Guru"
					if "admin_name" in g:
						g_admin_name = g["admin_name"]
					
					if not "admin_psw" in g:
						sendErrorToClient(client, ERROR_GAME_MISSADMINPSW, resp_lng)# Game error: miss admin password
						continue
					g_admin_psw = g["admin_psw"]
					
					g_consts = CONSTS.copy()
					if "consts" in g:
						g_consts = g["consts"]
					
					g_public = True
					if "public" in g:
						g_public = g["public"]
					
					if not "name" in g:
						sendErrorToClient(client, ERROR_GAME_MISSNEWGAMENAME, resp_lng)# Game error: miss new game name
						continue
					if g["name"] in games_index:
						sendErrorToClient(client, ERROR_GAME_USEDGAMENAME, resp_lng)# Game error: that game name is already used
						continue
					game = Game(g["name"], g_public, g_admin_name, g_admin_address, g_admin_psw, g_consts)
					games.append(game)
					games_index[game.name] = game
					resp["new_game_info"] = {"name":game.name, "public":game.public, "creation_time":game.creation_time, "admin_name":game.admin_name, "admin_address":game.admin_address, "consts":game.C}
				
				if "edit_game" in p_data:
					logPrint("Received edit game command", LOG_TRACE)
					p_resp = True
					if not "game_name" in p_data:
						sendErrorToClient(client, ERROR_GAME_MISSGAMENAME, resp_lng)# Game error: miss game name
						continue
					g_name = p_data["game_name"]
					
					if not g_name in games_index:
						sendErrorToClient(client, ERROR_GAME_GAMENOEXIST, resp_lng)# Game error: there is no game with this name
						continue
					game = games_index[g_name]
					
					g_admin_psw = ""
					if "admin_psw" in p_data:
						g_admin_psw = p_data["admin_psw"]
					if g_admin_psw != game.admin_psw:
						sendErrorToClient(client, ERROR_GAME_WRONGADMINPSW, resp_lng)# Game error: wrong admin password
						continue
					
					g = p_data["edit_game"]
					
					if "admin_name" in g:
						game.admin_name = g["admin_name"]
					if "admin_address" in g:
						game.admin_address = (g["admin_address"], game.admin_address[1])
					if "admin_port" in g:
						game.admin_address = (game.admin_address[0], g["admin_port"])
					if "admin_psw" in g:
						game.admin_psw = g["admin_psw"]
					if "public" in g:
						game.public = g["public"]
					if "consts" in g:
						game.C = g["consts"].copy()
					
					resp["edited_game_info"] = {"name":game.name, "public":game.public, "creation_time":game.creation_time, "admin_name":game.admin_name, "admin_address":game.admin_address, "consts":game.C}
				
				if "rm_game" in p_data:
					logPrint("Received remove game command", LOG_TRACE)
					p_resp = True
					
					if not "game_name" in p_data:
						sendErrorToClient(client, ERROR_GAME_MISSGAMENAME, resp_lng)# Game error: miss game name
						continue
					g_name = p_data["game_name"]
					
					if not g_name in games_index:
						sendErrorToClient(client, ERROR_GAME_GAMENOEXIST, resp_lng)# Game error: there is no game with this name
						continue
					game = games_index[g_name]
					
					g_admin_psw = ""
					if "admin_psw" in p_data:
						g_admin_psw = p_data["admin_psw"]
					if g_admin_psw != game.admin_psw:
						sendErrorToClient(client, ERROR_GAME_WRONGADMINPSW, resp_lng)# Game error: wrong admin password
						continue
					
					games_index.pop(game.name)
					games.remove(game)
					game = None
					
					resp["rm_game_done"] = True
				
				if "new_turn" in p_data:
					logPrint("Received new turn command", LOG_TRACE)
					p_resp = True
					
					try:
						g_name = getGameName(p_data, client, resp_lng)
						game = getGame(g_name, client, resp_lng)
					except ContinueError:
						continue
					
					g_admin_psw = ""
					if "admin_psw" in p_data:
						g_admin_psw = p_data["admin_psw"]
					if g_admin_psw != game.admin_psw:
						sendErrorToClient(client, ERROR_GAME_WRONGADMINPSW, resp_lng)# Game error: wrong admin password
						continue
					
					game.newTurn()
					resp["new_turn_done"] = {"turn":game.turn}
				
				if "new_player" in p_data:
					logPrint("Received new player command", LOG_TRACE)
					p_resp = True
					
					if not "game_name" in p_data:
						sendErrorToClient(client, ERROR_GAME_MISSGAMENAME, resp_lng)# Game error: miss game name
						continue
					g_name = p_data["game_name"]
					
					if not g_name in games_index:
						sendErrorToClient(client, ERROR_GAME_GAMENOEXIST, resp_lng)# Game error: there is no game with this name
						continue
					game = games_index[g_name]
					
					g = p_data["new_player"]
					
					g_player_name = ""
					if "player_name" in g:
						g_player_name = g["player_name"]
					
					g_player_address = (addr[0],8654)
					if "player_address" in g:
						g_player_address = (g["player_address"], g_player_address[1])
					if "player_port" in g:
						g_player_address = (g_player_address[0], g["player_port"])
					
					g_player_psw = ""
					if "player_psw" in g:
						g_player_psw = g["player_psw"]
					
					player = game.addPlayer(g_player_name, g_player_address, g_player_psw)
					
					resp["new_player_done"] = {"player_name":player.name, "player_uid":player.uid, "player_address":player.address[0], "player_port":player.address[1]}
				
				if "rm_player" in p_data:
					logPrint("Received remove player command", LOG_TRACE)
					p_resp = True
					
					if not "game_name" in p_data:
						sendErrorToClient(client, ERROR_GAME_MISSGAMENAME, resp_lng)# Game error: miss game name
						continue
					g_name = p_data["game_name"]
					
					if not g_name in games_index:
						sendErrorToClient(client, ERROR_GAME_GAMENOEXIST, resp_lng)# Game error: there is no game with this name
						continue
					game = games_index[g_name]
					
					g = p_data["rm_player"]
					
					g_player_uid = None
					if "player_uid" in g:
						g_player_uid = g["player_uid"]
					
					if not g_player_uid in game.players_index:
						sendErrorToClient(client, ERROR_GAME_PLAYERNOEXIST, resp_lng)# Game error: that player does not exist
						continue
					
					if "admin_psw" in g:
						g_admin_psw = g["admin_psw"]
						if g_admin_psw != game.admin_psw:
							sendErrorToClient(client, ERROR_GAME_WRONGADMINPSW, resp_lng)# Game error: wrong admin password
							continue
					elif "player_psw" in g:
						g_player_psw = g["player_psw"]
						if g_player_psw != game.players_index[g_player_uid].psw:
							sendErrorToClient(client, ERROR_GAME_WRONGPLAYERPSW, resp_lng)# Game error: wrong player password
							continue
					else:
						sendErrorToClient(client, ERROR_GAME_MISSPLAYERPSW, resp_lng)# Game error: miss player password
						continue
					
					player = game.removePlayer(g_player_uid)
					
					resp["rm_player_done"] = {"player_name":player.name, "player_uid":player.uid, "player_address":player.address[0], "player_port":player.address[1]}
					
					player = None
				
				if "exchanges" in p_data:
					logPrint("Received exchange command", LOG_TRACE)
					p_resp = True
					
					if not "game_name" in p_data:
						sendErrorToClient(client, ERROR_GAME_MISSGAMENAME, resp_lng)# Game error: miss game name
						continue
					g_name = p_data["game_name"]
					
					if not g_name in games_index:
						sendErrorToClient(client, ERROR_GAME_GAMENOEXIST, resp_lng)# Game error: there is no game with this name
						continue
					game = games_index[g_name]
					
					g = p_data["exchanges"]
					
					g_player_uid = None
					if "player_uid" in g:
						g_player_uid = g["player_uid"]
					
					if not g_player_uid in game.players_index:
						sendErrorToClient(client, ERROR_GAME_PLAYERNOEXIST, resp_lng)# Game error: that player does not exist
						continue
					
					if "player_psw" in g:
						g_player_psw = g["player_psw"]
						if g_player_psw != game.players_index[g_player_uid].psw:
							sendErrorToClient(client, ERROR_GAME_WRONGPLAYERPSW, resp_lng)# Game error: wrong player password
							continue
					else:
						sendErrorToClient(client, ERROR_GAME_MISSPLAYERPSW, resp_lng)# Game error: miss player password
						continue
					
					if not "exchanges_data" in g:
						sendErrorToClient(client, ERROR_GAME_MISSEXCHDATA, resp_lng)# Game error: miss exchanges data
						continue
					
					try:
						for exch in g["exchanges_data"]:
							
							# Check each transaction
							for exchterm in exch:
								assert len(exchterm) == 3
								if not exchterm[0] in game.players_index or not exchterm[1] in game.players_index:
									sendErrorToClient(client, ERROR_GAME_PLAYERNOEXIST, resp_lng)# Game error: that player does not exist
									raise ContinueError
								if type(exchterm[2]) == int or type(exchterm[2]) == float:
									assert exchterm[2] >= 0
								else:
									assert len(exchterm[2]) == game.C["N_VALUES"]
							
							# TODO: add to exchanges list, confirm, apply
					
					except ContinueError:
						continue
					except (TypeError, AssertionError):
						sendErrorToClient(client, ERROR_GAME_INVEXCHDATA, resp_lng)# Game error: invalid exchange data
						continue
					
			
			else:
				sendErrorToClient(client, ERROR_COM_LANGUAGE, resp_lng)# Communication error: unknown language
				continue
		
		if p_resp:
			if resp_lng == "json":
				resp_raw = json.dumps(resp).encode()
			client.sendall(encodeData(resp_raw, resp_lng))
		
		client.close()
